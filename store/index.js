export const state = () => ({
  drawer: false,
  pages: [
    {
      name: 'Blog',
      to: '/blog',
      icon: 'mdi-book-open-page-variant'
    }
  ],
  socialIcons: [
    {
      href: 'https://www.linkedin.com/in/jose-manuel-pe%C3%B1a-62733ba1/',
      icon: 'mdi-linkedin'
    },
    {
      href: 'https://github.com/JoseMPena',
      icon: 'mdi-github-face'
    },
    {
      href: 'https://gitlab.com/JosMPena',
      icon: 'mdi-gitlab'
    },
    {
      href: 'https://twitter.com/JosMPena',
      icon: 'mdi-twitter'
    }
  ]
})

export const mutations = {
  setDrawer: (state, payload) => (state.drawer = payload),
  toggleDrawer: state => (state.drawer = !state.drawer)
}
