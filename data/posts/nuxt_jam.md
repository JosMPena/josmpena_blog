---
title: Simple Blog with Nuxtjs and Markdown
date: 12/02/2020
category: FrontEnd
img: snowcup.jpg
filename: nuxt_jam
---

Finally got the courage to stop making excuses and start a blog. The main reason for it is that, as a developer, I'm constantly playing with different things, and keeping track of all this in your head is usually not a good idea. Specially if, like myself, you tend to have a bad memory.
Of course I could just start writing at any of the existing platform such as Medium and the likes but, as said,
I'm a developer, not a writer so what would be the fun of "just write"? Why can't I write in my own platform?.

So, reading around I stumbled into [This post](https://marinaaisa.com/blog/blog-using-vue-nuxt-markdown/) and yeah, it sounds pretty good so lets build something similar!

I found Nuxtjs to be a nice framework built upon <italic>another framework<italic>: Vuejs. Anyway, it makes easier to create client-side applications using Vue's templating system. Nuxt comes with a nice scaffolding tool to quickly create applications, (more info in [Their site](https://nuxtjs.org/guide/installation)). So go ahead and create your Nuxt application with ``$ yarn create nuxt-app <project-name>`` 

PD: Make sure to [have yarn installed](https://classic.yarnpkg.com/en/docs/install)

In this case, I chose to use Vuetify for the styling, but any other option such as Bootstrap, Bulma, etc. would work just fine

Once we have our project initiated, we would see there is a ``pages`` folder. This is the that will hold our pages or <italic>views<italic> as files with the ``.vue`` extension. A cool feature about Nuxtjs is that it will automagically create a path based on the files in this directory. I.e: we have the ``index.vue`` file by default, which will be the root page. If you create a ``blog.vue`` file, Nuxt will create the ``/blog`` path in your domain for you. This is useful to have each page(or <italic>view) as a separate component.

For this project, lets create a ``blog.vue`` file (again, you can simply use ``index.vue`` by overwriting its content) with the following code:
```javascript
<template>
  <v-content>
    <v-container>
      <div id="blog">
        <articles></articles>
      </div>
    </v-container>
  </v-content>
</template>

<script>
export default {
  components: {
    Articles: () => import("@/components/home/Articles")
  },

  head() {
    return {
      title: "Blog"
    };
  }
};
</script>
```

If you'r familiar with Vuejs, this pattern shouldn't look strange at all. What we're doing here is creating a Single Page Component, an artifact of Vue that allows you to have the template, the javascript and the styles of a specific component in a single file (We're not gonna go into details regarding Vuejs structure, you can find more info directly on [the VueJS site](https://vuejs.org/))

TL;DR: the ``<template>`` tag tells Vue that this will be the HTML markup of the component. The container of the blog post's previews. Note thet here we're using the ``v-content`` and ``v-container`` component from Vuetify and a ``articles`` component that we will create later on.

We also have the ``<script>`` tag, this is telling Vue that it will enclose the component's behaviour in Javascript, actually, we can use here any Javascript code, but we will use some Vue goodies.
This script tag will excport a Javascript object consisting on:
* A <italic>components</italic> object in which we're importing the ``Articles`` component that we're using in our previous ``Blog`` component's template.
* A ``head()`` function (this one provided by Nuxtjs) that should return an object with values to be provided to Nuxt's main head function when this particular component is rendered. In this case, we're simply telling Nuxt to put the value "Blog" in the page's title, in the browser tab (check the ``nuxt.config.js`` file, the head object, to see how this value will be treated).

We're skipping the addition of a ``style`` tag in this component as we will let the default styling from Vuetify.

Next, we're gonna create an ``Articles.vue`` file for our <italic>articles</italic> component.

## The posts feed
Let's create the layout of our posts' preview tiles. We're gonna use the following code for our ``template`` tag:
```javascript
<template>
  <section>
    <v-container grid-list-xl>
      <v-layout wrap>
        <feed-card
          v-for="(post, i) in posts"
          :key="post.attributes.title"
          :size="layout[i]"
          :value="post"
        />
      </v-layout>
    </v-container>
  </section>
</template>
```
Here, inside a ``section`` tag, we're going to use  the ``v-container`` component from Vuetify as a grid list (check Vuetify documentation for more info about this attribute), the ``v-layout`` component also from Vuetify to wrap our content and a custom component ``FeedCard`` that we are going to create next.
In the FeedCard's component tag, ``feed-card`` we are using the ``v-for`` directive (more info in Vuejs documentation) to iterate over a collection of posts. What this construct does can be summarised as follows:
- "For each element inside the posts collection, create an instance of FeedCard component and bind the arributes key, size and value in each case"
We will come back to these bindings when creating our FeedCard component.

The Javascript logic for this component (so far) would be as follows

```javascript

<script>

export default {
  components: {
    // import the FeedCard component to be used here
    FeedCard: () => import("@/components/FeedCard")
  },

  data: () => ({
    posts: [],
    // artificial layout representing the size of the preview tiles
    layout: [2, 2, 1, 2, 2, 3, 3, 3, 3, 3, 3]
  })
};
</script>

```

We will start off by importing the FeedCard component to be used in the template, also, as data for this component, we will start with an empty posts collection (we will populate it later), and a layout array that will define the size of the FeedCard as we iterate over the posts.

For now, let's just go over the rest of the components so we can come back to the posts rendering part, that is the interesting bit.

For the FeedCard component, create the ``FeedCard.vue`` file with the following content:

```javascript
<template>
  <v-flex xs12 :class="classes">
    <v-card tile flat color="grey lighten-1" dark :href="'/posts/' + postAttrs.name">
      <v-img
        :src="require(`@/assets/articles/${postAttrs.img}`)"
        height="100%"
        gradient="rgba(0, 0, 0, .42), rgba(0, 0, 0, .42)"
      >
        <v-layout v-if="!postAttrs.prominent" fill-height wrap ma-0>
          <v-flex xs12>
            <v-chip
              label
              class="mx-0 mb-2 text-uppercase"
              color="grey darken-3"
              text-color="white"
              small
              @click.stop
            >{{ postAttrs.category }}</v-chip>
            <h3 class="title font-weight-bold mb-2 white--text">{{ postAttrs.title }}</h3>
            <div class="caption">{{ postAttrs.date }}</div>
            <div v-html="highlight()"></div>
          </v-flex>
          <v-flex align-self-end>
            <v-chip
              class="text-uppercase ma-0 float-right"
              color="primary"
              label
              small
              @click.stop
            >Read More</v-chip>
          </v-flex>
        </v-layout>
      </v-img>
    </v-card>
  </v-flex>
</template>

<script>
export default {
  props: {
    size: {
      type: Number,
      required: true
    },
    value: {
      type: Object,
      default: () => ({})
    }
  },

  computed: {
    classes() {
      return {
        md6: this.size === 2,
        md4: this.size === 3
      };
    },
    postAttrs() {
      return this.value.attributes;
    }
  },

  methods: {
    highlight: function() {
      let firstParagraph = this.value.html.split("<p>")[1];
      return firstParagraph.substring(0, 512) + "...";
    }
  }
};
</script>

<style>
.v-image__image {
  transition: 0.3s linear;
}
</style>


```

### Rendering the previews
So, for each blog post we write, we want to display a ``v-card`` component with some information about the post as well as a cool image. This is exactly what our ``Articles`` component does. The problem here is that we are going to write our posts in [Markdown](https://www.markdownguide.org/) so we will need to tell Articles component where to look for these Markdown files and how to display the small info we need for the cards.

To solve this conundrum, let's change the content of the script tag in Articles component with the following code:
```javascript
<script>
import postsIndex from "~/data/postsIndex.json";

export default {
  components: {
    FeedCard: () => import("@/components/FeedCard")
  },

  data: () => ({
    posts: [],
    layout: [2, 2, 1, 2, 2, 3, 3, 3, 3, 3, 3]
  }),

  created() {
    Promise.all(
      postsIndex.map(post => {
        return import(`~/data/posts/${post.filename}`).then(response => {
          return { attributes: response.attributes, html: response.html };
        });
      })
    ).then(response => {
      this.posts = response;
    });
  }
}
</script>
```
We have created a ``postsIndex.json`` file under ``~/data/`` folder. This file is just an array of objects containing the filename which will help us import the actual Markdown file, by its name. The content of  ``postsIndex.json`` file will be as follows:
```javascript
[
  {
    "filename": "nuxt_jam.md"
  }
]

```

We also use the ``created()`` hook from Nuxtsj. The code enclosed in this hook will be executed <bold>after</bold> the component has been created.
Here, inside the created() hook we are iterating over the array of names in postsIndex.json and asyncronously loading the Markdown file whose name matches the filename property, this way we can populate the ``posts`` attribute of this component with objects containing the attributes and also the HTML parsed version of the Markdown markup. 
For Nuxt to properly recognise and load Markdown files, I included the ``frontmatter-markdown-loader`` in our project (https://github.com/hmsk/frontmatter-markdown-loader)

As a convention, our Markdown posts would have some key attributes at the beginning of the file, such as:
```yaml
---
title: Simple Blog with Nuxtjs and Markdown
date: 12/02/2020
category: FrontEnd
img: snowcup.jpg
filename: nuxt_jam
---
```

At this point, we should be able to see a grid of tiles, one per each Markdown file in our ``~/data/posts/`` folder, the name of such files should also be included in our ``postsIndex.json`` file.

### Rendering the Posts content
As we saw earlier, each of the preview tiles for our posts is actually a link to the filename under a ``/posts/`` namespace:
```javascript
<v-card tile flat color="grey lighten-1" dark :href="'/posts/' + postAttrs.name">
```

So we now have to tell Nuxt to listen to requests with such URL and render a view with the post's content as response.
One option to tell Nuxt to recognise such URL is adding an entry such as ``/posts/my_new_post`` in the ``routes`` key in ``nuxt.config.js`` file. But this is hardcoding is less than ideal.
Luckily for us, Nuxt provides a way to deal with this in a more dynamic way.

As we mentioned at the beginning, the ``pages`` directory in Nuxt has a special meaning. For each .vue file we create in this directory, Nuxt will automagically generate a path to this file, using the parameterized file name as URL. Additionally, every subdirectory inside ``pages`` works as a route namespace.
Having this 2 features in mind, we can proceed as follows:
* For the ``/posts/`` namespace in the URL, we create a ``posts`` directory inside ``pages``
* Then, in the ``posts`` folder, we can create a ``_slug.vue`` file. This is our way to tell Nuxt to try and listen for a variable <italic>slug</italic> (this can be any value, really) under the ``/posts/`` routes. The '_' at the beginning of the name tells Nuxt to expect any value in the path, not only the literal 'slug'. This way Nuxt will try and use the ``_slug.vue`` file to generate a response to any request in the format ``/posts/any_name_here``.

Our ``_slug.vue`` file would contain the following code:

```javascript
<template>
  <v-content>
    <v-container>
      <v-row class="mb-6">
        <v-col>
          <h1>{{ postAttrs.title }}</h1>
          <small>Posted: {{ postAttrs.date }}</small>
          <v-img
            class="mt-6 mb-6"
            :src="require(`@/assets/articles/${postAttrs.img}`)"
            height="300px"
            gradient="rgba(0, 0, 0, .42), rgba(0, 0, 0, .42)"
          ></v-img>
          <div v-html="postContent"></div>
        </v-col>
      </v-row>
    </v-container>
  </v-content>
</template>

<script>
export default {
  validate({ params }) {
    return params.slug.length;
  },

  async asyncData({ params }) {
    async function asyncImport(postName) {
      return await import(`~/data/posts/${postName}.md`);
    }

    return Promise.resolve(asyncImport(params.slug)).then(res => {
      return {
        postContent: res.html,
        postAttrs: res.attributes
      };
    });
  },

  head () {
    return {
      title: this.postAttrs.title
    }
  }
};
</script>
```

The important bits here are:
- The ``validate()`` hook, which evaluates an expression that should return a truthy value (i.e: not false or undefined). This is used to ensure that the URL contains some value for the file name in the URL ``/posts/my_file_name``
- The ``asyncData()`` function from Nuxtjs allows the component to have a ``data()`` dataset that will be created asyncrhonously based on the return object in ``Promise.resolve``.
- The ``head()`` function here defines a title value that will be appended to the browser's tab title. 
- In the template, the ``<div v-html="postContent"></div>`` line tells VueJS to inject the parsed HTML from postContent inside the div.

With this, we should now be able to click on the post tiles and render the actual post contents.

:D